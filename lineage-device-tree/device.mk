#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    qca6234-service.sh \
    init.qcom.early_boot.sh \
    init.qcom.efs.sync.sh \
    install-recovery.sh \
    init.qcom.sensors.sh \
    record_playback.sh \
    init.qcom.usb.sh \
    init.qcom.sh \
    init.qcom.post_boot.sh \
    init.class_main.sh \
    init.crda.sh \
    init.qti.dcvs.sh \
    init.qti.qcv.sh \
    init.qcom.class_core.sh \
    record.sh \
    init.qcom.coex.sh \
    loopback.sh \
    init.qcom.sdio.sh \
    init.qti.chg_policy.sh \
    init.mdm.sh \
    playback.sh \
    mishow_j.sh \

PRODUCT_PACKAGES += \
    fstab.qcom \
    init.target.rc \
    init.qti.ufs.rc \
    init.qcom.rc \
    init.qcom.usb.rc \
    exfat.rc \
    init.qcom.factory.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/xiaomi/ginkgo/ginkgo-vendor.mk)
